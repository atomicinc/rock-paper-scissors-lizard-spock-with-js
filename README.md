# Rock-Paper-Scissors-Lizard-Spock-with-JS
Classic game with a big bang twist. Written in Javascript.

See a live version at www.rockpaperscissorslizardspock.online

Development services and additional portfolio pieces at www.atomicgrowth.co/our-work/
